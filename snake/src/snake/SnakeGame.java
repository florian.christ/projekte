package snake;


import gui.Window;

import java.util.Random;

public class SnakeGame {
    private final Snake snake = new Snake();
    private Position food;

    public SnakeGame() {
        placeNewFood();
    }

    public void placeNewFood() {
        do {
            Random random = new Random();
            int x = random.nextInt((int) (SnakeApp.WIDTH / SnakeApp.FACTOR));
            int y = random.nextInt((int) (SnakeApp.HEIGHT / SnakeApp.FACTOR));
            food = new Position(x, y);

        } while (snake.getBody().contains(food));
    }

    public void step(Window window) {
        snake.move();

        for (Position p : snake.getBody()) {
            if (snake.nextHeadPosition().equals(p)) {
                SnakeApp.gameOver(window);
            }

            if (snake.nextHeadPosition().getX() < -2
                    || snake.nextHeadPosition().getY() < -2
                    || snake.nextHeadPosition().getX() > (int) (SnakeApp.WIDTH / SnakeApp.FACTOR) + 1
                    || snake.nextHeadPosition().getY() > (int) (SnakeApp.HEIGHT / SnakeApp.FACTOR) + 1) {
                SnakeApp.gameOver(window);
            }
        }

        if (snake.nextHeadPosition().equals(food)) {
            snake.grow();
            placeNewFood();
            SnakeApp.setWaitTime((int) (SnakeApp.getWaitTime() * 0.95));
            SnakeApp.setScore(SnakeApp.getScore() + 1);
        }
    }

    public Snake getSnake() {
        return snake;
    }

    public Position getFood() {
        return food;
    }
}
