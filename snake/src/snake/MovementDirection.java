package snake;

public enum MovementDirection {
    NORTH,
    EAST,
    SOUTH,
    WEST;
}
