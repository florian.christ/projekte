package snake;

import java.util.ArrayList;
import java.util.List;

public class Snake {
    private MovementDirection movementDirection;
    private final List<Position> body = new ArrayList<>();


    public Snake() {
        movementDirection = MovementDirection.NORTH;
        body.add(new Position(20, 10));
        body.add(new Position(20, 11));
        body.add(new Position(20, 12));
    }

    public void move() {
        switch (movementDirection) {
            case NORTH -> {
                body.add(0, new Position(body.get(0).getX(), body.get(0).getY() - 1));
                body.remove(body.size() - 1);
            }

            case EAST -> {
                body.add(0, new Position(body.get(0).getX() + 1, body.get(0).getY()));
                body.remove(body.size() - 1);
            }

            case SOUTH -> {
                body.add(0, new Position(body.get(0).getX(), body.get(0).getY() + 1));
                body.remove(body.size() - 1);
            }

            case WEST -> {
                body.add(0, new Position(body.get(0).getX() - 1, body.get(0).getY()));
                body.remove(body.size() - 1);
            }
        }
    }

    public void grow() {
        switch (movementDirection) {
            case NORTH -> body.add(0, new Position(body.get(0).getX(), body.get(0).getY() - 1));

            case EAST -> body.add(0, new Position(body.get(0).getX() + 1, body.get(0).getY()));

            case SOUTH -> body.add(0, new Position(body.get(0).getX(), body.get(0).getY() + 1));

            case WEST -> body.add(0, new Position(body.get(0).getX() - 1, body.get(0).getY()));
        }
    }

    public Position nextHeadPosition() {
        switch (movementDirection) {
            case NORTH -> {
                return new Position(body.get(0).getX(), body.get(0).getY() - 1);
            }

            case EAST -> {
                return new Position(body.get(0).getX() + 1, body.get(0).getY());
            }

            case SOUTH -> {
                return new Position(body.get(0).getX(), body.get(0).getY() + 1);
            }

            case WEST -> {
                return new Position(body.get(0).getX() - 1, body.get(0).getY());
            }
        }

        return null;
    }

    public List<Position> getBody() {
        return body;
    }

    public MovementDirection getMovementDirection() {
        return movementDirection;
    }

    public void setMovementDirection(MovementDirection movementDirection) {
        this.movementDirection = movementDirection;
    }
}
