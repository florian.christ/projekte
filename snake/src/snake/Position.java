package snake;

public class Position {
    private final int x;
    private final int y;

    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public boolean equals(Object other) {

        if (!(other instanceof Position)) return false;

        return x == ((Position) other).getX()
                && y == ((Position) other).getY();
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

}
