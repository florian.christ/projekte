package snake;


import gui.Window;

public class SnakeApp {
    public static final int WIDTH = 900;
    public static final int HEIGHT = 600;
    public static final double FACTOR = 30;
    private static final SnakeGame game = new SnakeGame();
    private static int waitTime = 400;
    private static int score = 0;


    public static void main(String[] args) {
        Window window = new Window("Snake", WIDTH, HEIGHT);
        window.open();

        while (window.isOpen()) {
            controls(window);
            drawSnake(window);
            drawFood(window);
            drawScore(window);
            game.step(window);
            window.refreshAndClear(waitTime);
        }

    }

    public static void drawSnake(Window window) {
        for (Position p : game.getSnake().getBody()) {
            window.setStrokeWidth(2);
            window.setColor(0, 0, 0);
            window.fillRect(p.getX() * FACTOR, p.getY() * FACTOR, FACTOR, FACTOR);
            window.setColor(255, 255, 255);
            window.drawRect(p.getX() * FACTOR, p.getY() * FACTOR, FACTOR, FACTOR);
        }
    }

    public static void drawFood(Window window) {
        window.setStrokeWidth(2);
        window.setColor(0, 0, 0);
        window.fillRect(game.getFood().getX() * FACTOR, game.getFood().getY() * FACTOR, FACTOR, FACTOR);
        window.setColor(255, 255, 255);
        window.drawRect(game.getFood().getX() * FACTOR, game.getFood().getY() * FACTOR, FACTOR, FACTOR);
    }

    public static void gameOver(Window window) {
        window.setColor(255, 255, 255);
        window.fillRect(0, 0, WIDTH, HEIGHT);
        window.setColor(255, 0, 0);
        window.setFontSize(30);
        window.drawStringCentered("Game Over", (double) WIDTH / 2, (double) HEIGHT / 2);
        window.refreshAndClear();
        window.waitUntilClosed();
    }

    public static void drawScore(Window window) {
        window.setColor(0, 0, 0);
        window.setFontSize(20);
        window.drawString("Score: " + score, 20, 20);
    }

    public static void controls(Window window) {
        if (window.wasKeyTyped("UP" )
                && game.getSnake().getMovementDirection() != MovementDirection.SOUTH) {
            game.getSnake().setMovementDirection(MovementDirection.NORTH);
        }

        if (window.wasKeyTyped("RIGHT")
                && game.getSnake().getMovementDirection() != MovementDirection.WEST) {
            game.getSnake().setMovementDirection(MovementDirection.EAST);
        }

        if (window.wasKeyTyped("DOWN")
                && game.getSnake().getMovementDirection() != MovementDirection.NORTH) {
            game.getSnake().setMovementDirection(MovementDirection.SOUTH);
        }

        if (window.wasKeyTyped("LEFT")
                && game.getSnake().getMovementDirection() != MovementDirection.EAST) {
            game.getSnake().setMovementDirection(MovementDirection.WEST);
        }
    }

    public static int getWaitTime() {
        return waitTime;
    }

    public static void setWaitTime(int waitTime) {
        SnakeApp.waitTime = waitTime;
    }

    public static int getScore() {
        return score;
    }

    public static void setScore(int score) {
        SnakeApp.score = score;
    }
}
