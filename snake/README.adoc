= Snake (OOP1)
:idprefix: user-content-
:idseparator: -
:stem: latexmath
:icons: font
:hide-uri-scheme:
:figure-caption!:

[.text-center]
image::res/snake.gif[width=600]

Programmieren Sie Ihr eigenes Snake! 1976 zum ersten Mal als https://en.wikipedia.org/wiki/Blockade_(video_game)[«Blockade»] veröffentlicht, wurde das Spiel um die Jahrtausendwende dank dem legendären https://de.wikipedia.org/wiki/Nokia_3310[Nokia 3310] zum Massenphänomen. Simpel, aber mit grossen Suchtfaktor!

Das Ziel des Spiels ist, mit der Schlange möglichst viel Futter zu fressen, ohne in den Rand oder den eigenen Körper zu steuern. Gar nicht mal so einfach, denn die Schlange wird mit jedem Stück Futter länger und schneller. Die Schlange bewegt sich dabei von alleine, als Spieler/in kann man lediglich versuchen, rechtzeitig die Richtung zu ändern.

*Themen:* Klassen, Enums, Listen, `Random`-Klasse, Grafik


:sectnums:
== Grundklassen

Snake spielt auf einem zweidimensionalen Spielfeld, das aus stem:[w \times h] einzelnen Feldchen besteht, auf welchen sich die Schlange bewegt und Futter erscheint:

[.text-center]
image::res/game-coordinates.svg[width=400]

Erstellen Sie als Erstes zwei Grundklassen, welche die Arbeit auf diesem Spielfeld erleichtern:

* Eine Klasse für einzelne Positionen auf dem Spielfeld. Jede Position besteht aus einer stem:[x]- und stem:[y]-Koordinate, beides `int`-Werte. Solche Positions-Objekte können Sie in den nächsten Schritten verwenden, um das Futter und die einzelnen Körperstücke der Schlange zu repräsentieren.
* Eine Klasse für die möglichen Bewegungsrichtungen der Schlange. Da es genau vier davon gibt (Nord, Ost, Süd, West), bietet es sich an, eine Enum verwenden.

.**Empfehlungen**
[%collapsible]
====
* Auf den ersten Blick erscheint es sinnvoll, Gültigkeits-Checks in die Positions-Klasse einzubauen, welche z. B. prüfen, dass die Koordinaten nicht < 0 sind, da die Schlange und das Futter ja nie ausserhalb des Spielfelds sein können. Es kann aber nützlich sein, auch ungültige Positions-Objekte zuzulassen, z. B. um eine Position zu repräsentieren, wo die Schlange in einem nächsten Schritt landen _würde_; das macht es später u. U. leichter, die _Game-Over_-Bedingung zu implementieren.
* Es ist empfehlenswert, Positions-Objekte _unveränderlich_ zu machen. Das bedeutet, dass die Koordinaten eines Positions-Objekts nach dem Erstellen nicht mehr geändert werden können. Wenn man später eine bestimmte Position (z. B. die des Futters) ändern möchte, _ersetzt_ man das entsprechende Positions-Objekt, statt es zu _ändern_. Das ist ein übliches Vorgehen, wenn es um Objekte geht, welche man sich als _Wert_ vorstellt; denken Sie z. B. an ``String``s, welche auch unveränderlich sind und stets neue Objekte zurückgeben, wenn man eine Transformation wie `toLowerCase` macht.
* Fügen Sie eine `equals`-Methode zur Positions-Klasse hinzu; diese wird sich später als nützlich erweisen.
====


== Snake-Klasse

Als Nächstes erstellen Sie eine Klasse für die Schlange. Eine Schlange hat einen Körper und eine Bewegungsrichtung.

Es gibt verschiedene Möglichkeiten, den Körper der Schlange zu modellieren. Ein einfaches und elegantes Modell ist eine simple _Liste von Positionen_, wobei die erste Position dem Kopf der Schlange entspricht. Eine waagrechte Schlange der Länge 4 mit dem Kopf in der linken oberen Ecke des Spielfelds hätte entsprechend folgende Listen-Repräsentation:

[.text-center]
image::res/body-model-1.svg[width=400]

Wenn sich die Schlange übers Spielfeld bewegt, werden vorne in die Liste neue Positionen eingefügt, während die hinteren Positionen «rausfallen». Macht die Schlange z. B. eine Bewegung nach unten (Süd), passiert mit der Liste folgendes:

[.text-center]
image::res/body-model-2.svg[width=400]

Nach einer weiteren Bewegung nach unten:

[.text-center]
image::res/body-model-3.svg[width=400]

Implementieren Sie die Snake-Klasse, indem Sie eine `ArrayList` für den Körper verwenden; diese macht das Einfügen und Entfernen von Positionen denkbar einfach. Fügen Sie einen Getter für den Körper und einen Setter für die Bewegungsrichtung hinzu; dieser wird später aufgerufen, wenn man eine Richtungstaste auf der Tastatur drückt. Zwei weitere Methoden sollen die Schlange steuern:

* `move()`: Bewegt die Schlange in die aktuelle Bewegungsrichtung, wie oben dargestellt.
* `grow()`: Macht etwas Ähnliches wie `move()`, aber für den Fall, dass die Schlange ein Futterstück frisst. In diesem Fall bewegt sich die Schlange zwar auch um ein Feld nach vorne, aber gleichzeitig wird der Körper ein Stück länger, d. h. es fällt keine Position hinten aus der Liste raus.

.**Empfehlungen**
[%collapsible]
====
* Es empfiehlt sich, eine weitere Methode zur Snake-Klasse hinzuzufügen, welche die Position zurückgibt, auf welcher der Kopf der Schlange im nächsten Schritt landet, bzw. landen _würde_. Diese Information wird später benötigt, um festzustellen, ob die Schlange Futter frisst oder in den Rand oder den eigenen Körper steuert. Die Methode kann übrigens auch die Implementation von `move()` und `grow()` vereinfachen.
* Falls Sie bereits Erfahrung mit dem Schreiben von Unit-Tests haben, bietet es sich an, die Snake-Klasse zu testen. Ansonsten können Sie auch kleine Testprogramme (mit `main`-Methode) schreiben, wo Sie ein Snake-Objekt erstellen, Methoden darauf aufrufen und danach den Zustand der Schlange auf der Konsole ausgeben. Eventuell müssen Sie dafür `toString`-Methoden zu Ihren Klassen hinzufügen.
====


== Initialisierung & Darstellung

Als Nächstes implementieren Sie die Initialisierung und die Darstellung die Spiels. Versuchen Sie, das in zwei separaten Klassen zu machen. Eine Klasse `SnakeApp` mit einer leeren `main`-Methode ist bereits in der Vorlage vorhanden; diese soll für die Darstellung und die Steuerung via Tastatur zuständig sein. Eine weitere Klasse, z. B. namens `SnakeGame`, soll den Spielzustand enthalten und die Spielregeln implementieren.

Beginnen Sie mit dem Spielzustand; dieser beinhaltet vorerst die Grösse des Spielfelds, ein Snake-Objekt und die aktuelle Position des Futters. (Später können Sie noch Punktestand und Highscore ergänzen.)

Im Konstruktor der `SnakeGame`-Klasse können Sie die Initialisierung des Spiels implementieren. Es beginnt mit einem zufällig platzierten Futterstück und mit der Schlange in einer bestimmten Position (z. B. in der Mitte), Ausrichtung und Länge. Die Futter-Position können Sie mithilfe der vordefinierten `Random`-Klasse bestimmen.

Implementieren Sie anschliessend in `SnakeApp` eine simple Darstellung des Spiels, mithilfe der https://gitlab.fhnw.ch/2022hs-oop1/docs/-/blob/main/woche-07/GUI-Programmierung.pdf[Window-Klasse aus OOP1]. Sie können zu Beginn alles mit Quadraten (`fillRect`) machen:

[.text-center]
image::res/simple-presentation.png[width=500]

Denken Sie daran, dass Sie die Spielkoordinaten nicht 1:1 zum Zeichnen verwenden können; sonst wäre die Schlange ja nur 1 Pixel breit. Stattdessen definieren Sie am besten einen _Skalierungsfaktor_, der angibt, wie gross ein einzelnes Feldchen des Spiels im Fenster dargestellt wird, z. B. 30 Pixel. Mit diesem Faktor multiplizieren Sie dann alle stem:[x]- und stem:[y]-Koordinaten der Schlange und des Futters, um die GUI-Koordinaten der linken oberen Ecke des entsprechenden Feldchens zu erhalten. Innerhalb des Feldchens können Sie dann unterschiedlich grosse Quadrate für Futter und Körperstücke zeichnen.

.**Empfehlung**
[%collapsible]
====
Beim Platzieren des Futters sollte man korrekterweise darauf achten, dass die Position nicht bereits vom Körper der Schlange abgedeckt ist. Dies ist nicht ganz einfach, aber eine gute Anwendung einer `do`-`while`-Schleife: In der Schleife wird eine zufällige Position innerhalb des Spielfelds bestimmt; die Schleife prüft anschliessend, ob diese Position bereits von der Schlange abgedeckt wird und wiederholt den Schleifeninhalt allenfalls. Um zu bestimmen, ob eine Position vom Körper der Schlange abgedeckt ist, können Sie die `contains`-Methode von `ArrayList` verwenden (vorausgesetzt, Ihre Positions-Klasse hat eine sinnvolle `equals`-Methode).
====


== Spielregeln & Steuerung

Nun implementieren Sie die Spielregeln und die Steuerung. Fügen Sie dazu Ihrer `SnakeGame`-Klasse eine Methode hinzu, welche das Spiel um einen Schritt weiterbringt (z. B. namens `step`). In dieser Methode implementieren Sie die Spielregeln:

* Falls die Schlange als Nächstes (mit der aktuellen Bewegungsrichtung) in den Spielfeldrand oder den eigenen Körper steuern würde, ist _Game Over_. Die Bewegung wird nicht mehr durchgeführt, stattdessen wird signalisiert, dass das Spiel zu Ende ist. Das kann über einen Rückgabewert oder indirekt über ein Attribut und Getter gemacht werden.
* Andernfalls, falls die Schlange sich als Nächstes auf die Position mit dem Futter bewegt, wird der Körper um eins länger (`grow`) und das Futterstück wird neu platziert.
* Andernfalls macht die Schlange eine normale Bewegung (`move`).

In `SnakeApp` können Sie jetzt die Spielsteuerung implementieren. Die Grundstruktur ist eine `while`-Schleife, welche so lange läuft, bis das Spiel zu Ende ist und/oder das Fenster geschlossen wird. In der Schleife werden folgende Schritte durchgeführt:

. Es wird geprüft, ob bestimmte Tasten gedrückt wurden. Dies geht mit Aufrufen wie `window.wasKeyTyped("LEFT")`.
. Entsprechend der Tastatureingaben wird die Bewegungsrichtung der Schlange gesetzt. Falls nichts gedrückt wurde, bleibt die Bewegungsrichtung unverändert; die Schlange sollte sich einfach weiter geradeaus bewegen.
. Das Spiel wird einen Schritt weitergebracht, indem die oben implementierte `step`-Methode aufgerufen wird. Eine soeben gesetzte Bewegungsrichtung wird sofort berücksichtigt.
. Der neue Spielzustand wird ins Fenster gezeichnet.
. Das Fenster wird aktualisiert und es wird einen Moment gewartet, bevor der nächste Schleifendurchlauf gemacht wird. Diese wird mit einem Aufruf `window.refreshAndClear(waitTime)` gemacht, wobei `waitTime` eine Wartezeit in Millisekunden ist.

Sie sollten jetzt eine rudimentäre erste Version von Snake spielen können. Überprüfen Sie, dass alle Spielregeln korrekt implementiert sind.


== Feinschliff & Erweiterungen

Zum Schluss können Sie die Spielregeln verfeinern und die Grafik und User-Experience nach beliebigen verbessern. Hier einige Vorschläge:

* *Neustart:* Um für einen weiteren Versuch das Programm nicht jedes Mal neu starten zu müssen, können Sie eine weitere Schleife um die bisherige Hauptschleife setzen, welche nach einem _Game Over_ einfach ein neues Spiel initialisiert und startet. Diese Schleife soll so lange laufen, bis das Fenster geschlossen wird.
* *Schnellere Schlange:* Das Spiel wird interessanter, wenn die Schlange mit jedem gefressenen Futter etwas schneller wird, bzw. die Wartezeit zwischen den Spielschritten etwas kürzer. Sie könnten diese z. B. nach jedem Futterstück um 5 % verringern.
* *Punkte zählen:* Für jedes gefressene Futter gibt es einen Punkt. Um den Punktestand anzuzeigen, wandeln Sie ihn in einen String um und zeichnen ihn mittels `window.drawString(string, x, y)` ins Fenster.
* *Highscore:* Ein nächster Schritt wäre, den höchsten Punktestand über alle bisher gespielten Spiele zu speichern. (Um den Highscore auch nach dem Beenden des Programms zu behalten, müsste man diesen allerdings in eine Datei speichern. Der Umgang mit Dateien wird in OOP2 behandelt.)
* *Retro-Grafik:* Snake macht mehr Spass mit einer hübschen, pixeligen Retro-Grafik. Bei der Spielgrafik selbst ist das gar nicht so schwierig, beim Punktestand schon eher. Falls Sie die Herausforderung annehmen möchten, finden Sie in der Datei link:res/RetroFont.java[RetroFont.java] als kleine Hilfestellung die Bitmaps für die Ziffern 0 – 10, so wie sie in der Nokia-3310-Version von Snake erscheinen.
* *Multiplayer-Modus:* Im Original https://en.wikipedia.org/wiki/Blockade_(video_game)[«Blockade»] spielte man nicht alleine, sondern zu zweit gegeneinander! Ziel ist es, dem Gegner den Weg abzuschneiden und selbst nicht in ein Hindernis zu steuern.