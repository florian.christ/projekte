package colorful;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public class GemeindenData {
    private static final String DELIMITER = ";";
    private static final String FILE_NAME = "gemeindekennzahlen-2021.csv";
    private String title;
    private String year;
    private String columnTitle;
    private final Map<String, Double> dataMap = new HashMap<>();
    private double minValue;
    private double maxValue;

    public void readFile(int column) {

        try (BufferedReader reader = getReader(FILE_NAME)) {

            String line = reader.readLine();

            title = line.split(DELIMITER)[0];
            year = line.split(DELIMITER)[1];
            reader.readLine();

            line = reader.readLine();
            columnTitle = line.split(DELIMITER)[column];

            minValue = Double.POSITIVE_INFINITY;
            maxValue = Double.NEGATIVE_INFINITY;

            if (column > 1) {
                while ((line = reader.readLine()) != null) {
                    dataMap.putIfAbsent(line.split(DELIMITER)[1], Double.parseDouble(line.split(DELIMITER)[column]));
                    minValue = Math.min(minValue, Double.parseDouble(line.split(DELIMITER)[column]));
                    maxValue = Math.max(maxValue, Double.parseDouble(line.split(DELIMITER)[column]));
                }
            } else {
                throw new IOException();
            }



        } catch (IOException e) {
            throw new IllegalStateException("Failed to read file: " + e.getMessage());
        }

    }



    private BufferedReader getReader(String fileName) {
        InputStream inputStream = getClass().getResourceAsStream(fileName);
        assert inputStream != null;
        InputStreamReader reader = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
        return new BufferedReader(reader);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getColumnTitle() {
        return columnTitle;
    }

    public void setColumnTitle(String columnTitle) {
        this.columnTitle = columnTitle;
    }

    public Map<String, Double> getDataMap() {
        return dataMap;
    }

    public double getMinValue() {
        return minValue;
    }

    public void setMinValue(double minValue) {
        this.minValue = minValue;
    }

    public double getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(double maxValue) {
        this.maxValue = maxValue;
    }
}
