package colorful;


import java.awt.*;

public class ColorMixer {
    private double min;
    private double max;
    private Color minColor;
    private Color aveColor;
    private Color maxColor;

    public ColorMixer(double min, double max, Color minColor, Color aveColor, Color maxColor) {
        this.min = min;
        this.max = max;
        this.minColor = minColor;
        this.aveColor = aveColor;
        this.maxColor = maxColor;
    }

    public gui.Color mixColor(double value) {

        double average = (min + max) / 2;

        if (value < average) {
            double x = (value - min) / (average - min);
            int r = (int) ((1 - x) * minColor.getRed() + x * aveColor.getRed());
            int g = (int) ((1 - x) * minColor.getGreen() + x * aveColor.getGreen());
            int b = (int) ((1 - x) * minColor.getBlue() + x * aveColor.getBlue());
            return new gui.Color(r, g, b);
        } else {
            double x = (value - average) / (max - average);
            int r = (int) ((1 - x) * aveColor.getRed() + x * maxColor.getRed());
            int g = (int) ((1 - x) * aveColor.getGreen() + x * maxColor.getGreen());
            int b = (int) ((1 - x) * aveColor.getBlue() + x * maxColor.getBlue());
            return new gui.Color(r, g, b);
        }
    }

    public double getMin() {
        return min;
    }

    public double getMax() {
        return max;
    }

    public Color getMinColor() {
        return minColor;
    }

    public Color getAveColor() {
        return aveColor;
    }

    public Color getMaxColor() {
        return maxColor;
    }
}
