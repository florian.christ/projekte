package colorful;


import gui.Window;

import java.awt.*;
import java.awt.geom.Point2D;
import java.util.*;
import java.util.List;

public class ColorfulSwitzerland {
    private static final int WIDTH = 1000;
    private static final int HEIGHT = 700;
    private static final RegionReader REGION_READER = new RegionReader();
    private static Map<String, List<Point2D>> gemeinden;
    private static Map<String, List<Point2D>> kantone;
    private static double maxX;
    private static double minX;
    private static double maxY;
    private static double minY;
    private static final GemeindenData gemeindenData = new GemeindenData();
    private static ColorMixer colorMixer;
    private static String currentRegion;


    public static void main(String[] args) {

        gemeinden = REGION_READER.readFile("gemeindegebiet.txt");
        kantone = REGION_READER.readFile("kantonsgebiet.txt");

        calculateExtremeCoordinates(kantone);

        gemeindenData.readFile(18);

        Window window = new Window("Switzerland Map", WIDTH, HEIGHT);
        window.open();

        window.setFontSize(20);
        window.drawStringCentered(gemeindenData.getTitle(), (double) WIDTH / 2, 40);
        window.setFontSize(15);
        window.drawStringCentered(gemeindenData.getYear(), (double) WIDTH / 2, 60);

        colorMixer = new ColorMixer(gemeindenData.getMinValue(),
                gemeindenData.getMaxValue(),
                Color.decode("#ffd085"),
                Color.decode("#ee305c"),
                Color.decode("#330298"));

        drawLegend(window);

        for (Map.Entry<String, List<Point2D>> entry : gemeinden.entrySet()) {
            currentRegion = entry.getKey();
            drawArea(window, entry.getValue());
            window.setStrokeWidth(0.25);
            drawBorder(window, entry.getValue());
        }

        for (Map.Entry<String, List<Point2D>> entry : kantone.entrySet()) {
            window.setStrokeWidth(1.25);
            drawBorder(window, entry.getValue());
        }

        while (window.isOpen()) {
            window.refreshAndClear();
            window.waitUntilClosed();
        }

    }

    public static void drawArea(Window window, List<Point2D> region) {

        double[][] rings = createRings(region);

        if (gemeindenData.getDataMap().containsKey(currentRegion)) {
            window.setColor(colorMixer.mixColor(gemeindenData.getDataMap().get(currentRegion)));

        }

        window.fillMultiPolygon(rings);
    }

    public static void drawBorder(Window window, List<Point2D> region) {

        double[][] rings = createRings(region);

        window.setColor(255, 255, 255);
        window.drawMultiPolygon(rings);
    }

    private static double[][] createRings(List<Point2D> region) {
        List<Point2D> points = convertToGUICoordinates(region);

        double[][] rings = new double[1][points.size() * 2];
        int i = 0;
        for (Point2D point : points) {
            rings[0][i++] = point.getX();
            rings[0][i++] = point.getY();
        }

        return rings;
    }

    private static void calculateExtremeCoordinates(Map<String, List<Point2D>> regions) {
        maxX = Double.NEGATIVE_INFINITY;
        maxY = Double.NEGATIVE_INFINITY;
        minX = Double.POSITIVE_INFINITY;
        minY = Double.POSITIVE_INFINITY;

        for (List<Point2D> region : regions.values()) {
            for (Point2D point : region) {
                maxX = Math.max(maxX, point.getX());
                maxY = Math.max(maxY, point.getY());
                minX = Math.min(minX, point.getX());
                minY = Math.min(minY, point.getY());
            }
        }

    }

    public static List<Point2D> convertToGUICoordinates(List<Point2D> landCoordinates) {
        List<Point2D> guiCoordinates = new ArrayList<>();

        double xRange = maxX - minX;
        double yRange = maxY - minY;

        double xScale = (WIDTH - 100) / xRange;
        double yScale = (HEIGHT - 150) / yRange;

        for (Point2D landCoord : landCoordinates) {
            double guiX = (landCoord.getX() - minX + 50) * xScale + 50;
            double guiY = (HEIGHT - 50) - (landCoord.getY() - minY + 100) * yScale;

            guiCoordinates.add(new Point2D.Double(guiX, guiY));
        }

        return guiCoordinates;
    }

    public static void drawLegend(Window window) {
        int height = 20;
        int width = 200;
        int x = WIDTH - 125;
        int y = HEIGHT - 75;
        window.setFontSize(12);
        window.drawStringCentered(gemeindenData.getColumnTitle(), x, y);
        window.drawStringCentered(String.valueOf(gemeindenData.getMinValue()), x - (double) width / 2, y + 25);
        window.drawStringCentered(String.valueOf(gemeindenData.getMaxValue()), x + (double) width / 2, y + 25);

        ColorMixer cm = new ColorMixer(0, width, colorMixer.getMinColor(),
                colorMixer.getAveColor(),
                colorMixer.getMaxColor());

        for (int i = 0; i < width; i++){
            window.setColor(cm.mixColor(i));
            window.fillRect(x - (double) width / 2 + i, y + 35, 1, height);
        }
    }

}
