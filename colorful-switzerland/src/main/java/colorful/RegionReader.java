package colorful;

import java.awt.*;
import java.awt.geom.Point2D;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.List;

public class RegionReader {
    private static final String DELIMITER = ";";

    public Map<String, List<Point2D>> readFile(String fileName) {

        Map<String, List<Point2D>> regions = new TreeMap<>();

        try (BufferedReader reader = getReader(fileName)) {
            String line;
            String region = null;
            while ((line = reader.readLine()) != null) {
                if (line.isEmpty()) {
                    region = null;
                } else if (region == null) {
                    region = line.split(DELIMITER)[1];
                    regions.putIfAbsent(region, new ArrayList<>());
                } else {
                    String[] parts = line.split(DELIMITER);
                    if (parts.length == 2) {
                        int x = Integer.parseInt(parts[0]);
                        int y = Integer.parseInt(parts[1]);

                        regions.get(region).add(new Point(x, y));
                    }
                }
            }

        } catch (IOException e) {
            throw new IllegalStateException("Failed to read file: " + e.getMessage());
        }

        return regions;
    }

    private BufferedReader getReader(String fileName) {
        InputStream inputStream = getClass().getResourceAsStream(fileName);
        assert inputStream != null;
        InputStreamReader reader = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
        return new BufferedReader(reader);
    }
}
