package gameoflife;

import gui.Window;

public class Game {
    private final boolean[][] board = new boolean[50][50];
    private final int SIZE = 700;
    private final int CELLSIZE = SIZE / board.length;
    private final int STROKE = 1;

    public void step(Window window) {
        boolean[][] b = new boolean[board.length][board[0].length];
        for (int i = 0; i < board.length; i++) {
            System.arraycopy(board[i], 0, b[i], 0, board[i].length);
        }

        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board.length; j++) {
                if (board[i][j]) {
                    window.setColor(0, 0, 0);
                    window.fillRect(i * CELLSIZE + STROKE, j * CELLSIZE + STROKE, CELLSIZE - STROKE*2, CELLSIZE - STROKE*2);
                }

                boolean nextState;
                if (b[i][j]) nextState = checkLoneliness(b, i, j) && checkOverPopulation(b, i, j);
                else nextState = checkReproduction(b, i, j);

                setCell(i, j, nextState);
            }
        }

    }

    /*public int checkAround(boolean[][] b, int i, int j) {
        int count = 0;

        if (b[i-1][j]) count++;

        if (b[i+1][j]) count++;

        if (b[i][j-1]) count++;

        if (b[i][j+1]) count++;

        if (b[i+1][j+1]) count++;

        if (b[i-1][j+1]) count++;

        if (b[i-1][j-1]) count++;

        if (b[i+1][j-1]) count++;

        return count;
    }*/

    public int checkAround(boolean[][] b, int i, int j) {
        int count = 0;
        int rows = b.length;
        int cols = b[0].length;

        // Überprüfe Zelle oben
        if (b[(i - 1 + rows) % rows][j]) count++;

        // Überprüfe Zelle unten
        if (b[(i + 1) % rows][j]) count++;

        // Überprüfe Zelle links
        if (b[i][(j - 1 + cols) % cols]) count++;

        // Überprüfe Zelle rechts
        if (b[i][(j + 1) % cols]) count++;

        // Überprüfe Zelle unten rechts
        if (b[(i + 1) % rows][(j + 1) % cols]) count++;

        // Überprüfe Zelle oben rechts
        if (b[(i - 1 + rows) % rows][(j + 1) % cols]) count++;

        // Überprüfe Zelle oben links
        if (b[(i - 1 + rows) % rows][(j - 1 + cols) % cols]) count++;

        // Überprüfe Zelle unten links
        if (b[(i + 1) % rows][(j - 1 + cols) % cols]) count++;

        return count;
    }

    public boolean checkLoneliness(boolean[][] b, int i, int j) {
        int count = checkAround(b, i, j);

        return b[i][j] && count >= 2;
    }

    public boolean checkOverPopulation(boolean[][] b, int i, int j) {
        int count = checkAround(b, i, j);

        return b[i][j] && count <= 3;
    }

    public boolean checkReproduction(boolean[][] b, int i, int j) {
        int count = checkAround(b, i, j);

        /*if (!b[i][j]) return count == 3;*/

        return count == 3;
    }

    public void drawGrit(Window window) {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board.length; j++) {
                if (board[i][j]) {
                    window.setColor(0, 0, 0);
                    window.fillRect(i * CELLSIZE + STROKE, j * CELLSIZE + STROKE, CELLSIZE - STROKE*2, CELLSIZE - STROKE*2);
                }
            }
        }
    }

    public void radomStart() {
        for (int i = 15; i < board.length - 15; i++) {
            for (int j = 15; j < board.length - 15; j++) {
                double x = Math.random();
                if (x < 0.5) setCell(i, j, true);
            }
        }
    }

    public void setCell(int i, int j, boolean b) {
        board[i][j] = b;
    }

    public boolean[][] getBoard() {
        return board;
    }
}
