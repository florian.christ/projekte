package gameoflife;

import gui.Window;

public class GameOfLifeApp {
    private static final int SIZE = 700;
    private static final Game game = new Game();
    public static void main(String[] args) {
        Window window = new Window("Game of Life", SIZE, SIZE);
        window.open();

        game.radomStart();

        int waitTime = 250;

        while (window.isOpen()) {
            if (window.wasKeyTyped("SPACE")) window.waitUntilClosed();
            //game.drawGrit(window);
            //window.refresh(waitTime / 2);
            game.step(window);
            window.refreshAndClear(waitTime);
        }

    }

}
