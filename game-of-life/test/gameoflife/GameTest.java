package gameoflife;

import org.junit.jupiter.api.Test;

import java.lang.reflect.Array;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class GameTest {

    @Test
    void checkAround() {
        //given
        Game game = new Game();

        game.setCell(1, 1, true);
        game.setCell(1, 2, true);
        game.setCell(1, 3, true);
        game.setCell(2, 1, true);
        game.setCell(2, 2, true);
        game.setCell(2, 3, true);
        game.setCell(3, 1, true);
        game.setCell(3, 2, true);
        game.setCell(3, 3, true);

        //when
        int count = game.checkAround(game.getBoard(), 2, 2);

        //then
        assertEquals(8, count);

        //when
        game.setCell(1, 1, false);
        count = game.checkAround(game.getBoard(), 2, 2);

        //then
        assertEquals(7, count);
    }

    @Test
    void checkLoneliness() {
        //given
        Game game = new Game();
        game.setCell(2, 1, true);
        game.setCell(2, 2, true);
        game.setCell(2, 3, true);
        game.setCell(3, 3, true);

        //when
        boolean[][] b = game.getBoard().clone();
        for (int i = 1; i < game.getBoard().length-1; i++) {
            for (int j = 1; j < game.getBoard().length-1; j++) {
                game.setCell(i, j, game.checkLoneliness(b, i, j));
            }
        }

        //then
        assertFalse(game.getBoard()[2][1]);
        assertTrue(game.getBoard()[2][2]);
        assertTrue(game.getBoard()[2][3]);
        assertTrue(game.getBoard()[3][3]);
    }

    @Test
    void checkOverPopulation() {
        //given
        Game game = new Game();
        game.setCell(1, 2, true);
        game.setCell(1, 3, true);
        game.setCell(2, 2, true);
        game.setCell(2, 3, true);
        game.setCell(3, 2, true);
        game.setCell(3, 3, true);

        //when
        boolean[][] b = game.getBoard().clone();
        for (int i = 1; i < game.getBoard().length-1; i++) {
            for (int j = 1; j < game.getBoard().length-1; j++) {
                game.setCell(i, j, game.checkOverPopulation(b, i, j));
            }
        }

        //then
        assertFalse(game.getBoard()[2][2]);
        assertFalse(game.getBoard()[2][3]);
        assertTrue(game.getBoard()[1][2]);
        assertTrue(game.getBoard()[1][3]);
        assertTrue(game.getBoard()[3][2]);
        assertTrue(game.getBoard()[3][3]);
    }

    @Test
    void checkReproduction() {
        //given
        Game game = new Game();
        game.setCell(2, 2, true);
        game.setCell(2, 3, true);
        game.setCell(2, 4, true);
        game.setCell(3, 4, true);

        //when
        boolean[][] b = game.getBoard().clone();
        for (int i = 1; i < game.getBoard().length-1; i++) {
            for (int j = 1; j < game.getBoard().length-1; j++) {
                game.setCell(i, j, game.checkReproduction(b, i, j));
            }
        }

        //then
        assertTrue(game.getBoard()[1][3]);
        assertTrue(game.getBoard()[2][2]);
        assertTrue(game.getBoard()[2][3]);
        assertTrue(game.getBoard()[2][4]);
        assertTrue(game.getBoard()[3][4]);
    }


}