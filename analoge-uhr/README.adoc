= Analoge Uhr (OOP1)
:idprefix: user-content-
:idseparator: -
:stem: latexmath
:icons: font
:hide-uri-scheme:
:figure-caption!:

[.text-center]
image:res/analoge-uhr.gif[width=600]

In diesem Projekt implementieren Sie eine analoge Uhr mit Stunden-, Minuten- und Sekundenzeiger. Das Design und das Detailverhalten der Zeiger ist Ihnen überlassen; eine relativ einfache Uhr ist oben links dargestellt, die Bahnhofsuhr oben rechts braucht schon einiges an Hirnschmalz und Geduld. Auf jeden Fall brauchen Sie ein rudimentäres Verständnis von Winkelberechnungen und den trigonometrischen Funktionen stem:[\sin(\cdot)] und stem:[\cos(\cdot)].

*Themen:* Schleifen, arithmetische Ausdrücke, `Math`-Klasse, Grafik


:sectnums:
== Zeitmessung in Java

Erstellen Sie als Erstes einen Prototyp, der die aktuelle Uhrzeit einfach auf der Konsole ausgibt:

----
Aktuelle Uhrzeit: 14:50:58
----

Erweitern Sie dazu das Programm `AnalogeUhr` in der Vorlage für dieses Projekt. In Java können Sie mittels `System.currentTimeMillis()` auf die aktuelle Systemzeit zugreifen. Der Rückgabewert ist ein `long`-Wert, der die Millisekunden seit dem 01.01.1970 um 00:00:00 angibt (siehe https://de.wikipedia.org/wiki/Unixzeit[Unixzeit]).

Das Programm soll diesen unhandlichen Wert in menschenlesbare Form umwandeln; dazu brauchen Sie einige arithmetische Ausdrücke, inklusive dem Modulo-Operator (`%`), der den Rest einer Ganzzahldivision berechnet. Zum Beispiel gibt `1382 % 100` den Rest `82`.

Beachten Sie, dass `System.currentMillis()` die Zeit in der https://de.wikipedia.org/wiki/Koordinierte_Weltzeit[UTC]-«Zeitzone» zurückgibt. Um unsere Sommerzeit zu erhalten, muss man 2 Stunden zu diesem Wert addieren, für unsere Winterzeit 1 Stunde.


== Digitale Uhr

Erweitern Sie das Programm, sodass die Uhrzeit nicht einmalig in auf der Konsole ausgegeben, sondern kontinuierlich in einem Fenster anzeigt wird (aber vorerst weiterhin digital):

[.text-center]
image:res/digitale-uhr.gif[width=300]

Verwenden Sie dazu die https://gitlab.fhnw.ch/2022hs-oop1/docs/-/blob/main/woche-07/GUI-Programmierung.pdf[Window-Klasse aus OOP1]. Um immer wieder neue Inhalte im Fenster anzuzeigen, brauchen Sie eine `while`-Schleife und die Methode `window.refreshAndClear(waitTime)`, welche das Fenster leert und bereit macht für neue Zeichenoperationen. Das Programm soll so lange laufen, bis der Benutzer das Fenster schliesst (siehe Methode `window.isOpen()`).


== Zeiger zeichnen

Bauen Sie das Programm nun so um, dass statt der digitalen Zeit drei Zeiger gezeichnet werden. Beginnen Sie einfach: Jeder der Zeiger beginnt in der Mitte des Fensters und verläuft in einem Winkel nach aussen, der seinem Anteil der Zeit entspricht:

[.text-center]
image:res/analoge-uhr-einfach.gif[width=300]

Die Zeiger können Sie mit der Methode `window.drawLine(x1, y1, x2, y2)` zeichnen; dazu brauchen Sie die Start- und Endkoordinaten der Linie. Die Startkoordinaten sind trivial; aber für die Endkoordinaten brauchen Sie etwas Mathematik: Eine Zeitangabe, z. B. die Sekunden, wird zuerst in einen Winkel umgewandelt, der anschliessend anhand der trigonometrischen Funktionen stem:[sin(x)] und stem:[cos(x)] in Koordinaten umgerechnet wird. Folgendes Bild zeigt die geometrischen Zusammenhänge zwischen einem Winkel stem:[\theta] und den Längen stem:[x = \cos(\theta)] und stem:[y = \sin(\theta)]:

[.text-center]
image::res/sin-cos.svg[width=250]

Die Funktionen stem:[\sin(\cdot)] und stem:[\cos(\cdot)] stehen in Java durch die Klasse `Math` zur Verfügung: `Math.sin(winkel)`, `Math.cos(winkel)`. Beachten Sie jedoch, dass die Winkel für diese Methoden im https://de.wikipedia.org/wiki/Radiant_(Einheit)[Bogenmass (rad)] angegeben werden müssen; es gilt stem:[360^\circ = 2\pi]. Für die Umrechnung zwischen Grad und Bogenmass können Sie entweder die Methoden `Math.toRadians(degree)` und `Math.toDegrees(rad)` oder die Konstante `Math.PI` verwenden. Beachten Sie, dass stem:[\sin(\cdot)] und stem:[\cos(\cdot)] immer `double`-Werte zwischen `-1.0` und `1.0` liefern.


== Zifferblatt und Feinschliff

Als Letztes können Sie das Design der Uhr ausarbeiten. Zeiger können z. B. verschiedene Farben haben und unterschiedlich lang und breit sein. Zudem können Sie einen Hintergrund und ein Ziffernblatt hinzufügen, entweder mit Zahlen oder nur mit Strichen.

Als Extra-Herausforderung können Sie versuchen, das ikonische Design der Schweizer Bahnhofsuhr zu implementieren. Diese fällt vor allem dadurch auf, dass sich der Sekundenzeiger etwas schneller als bei anderen Uhren bewegt; nach jeder vollen Umdrehung https://de.wikipedia.org/wiki/Schweizer_Bahnhofsuhr#Pause_des_Sekundenzeigers_der_Schweizer_Bahnhofsuhr[bleibt er einen Moment stehen, bevor die neue Minute anbricht].

Bevor Sie dieses Projekt abschliessen, lassen Sie sich vom Sekundenzeiger der Bahnhofsuhr inspirieren und halten auch Sie kurz inne — um Ihren Code anzuschauen und etwaige Unschönheiten zu verbessern. Haben Sie z. B. Konstanten verwendet (`private static final int ...`) oder haben Sie lauter Zahlen im Code verteilt? Haben Sie gute Variablennamen verwendet? Ist die Einrückung konsistent? So viel Zeit muss sein!😉