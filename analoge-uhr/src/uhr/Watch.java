package uhr;


import gui.Window;

public class Watch {
    private static final int SIZE = 500;
    private static final int CENTRE = SIZE / 2;
    private static long hour;
    private static long minute;
    private static long second;
    private static long millisecond;

    public static void drawWatch(Window window) {
        hour = System.currentTimeMillis() / 1000 / 60 / 60 % 24 + 2;
        minute = System.currentTimeMillis() / 1000 / 60 % 60;
        second = System.currentTimeMillis() / 1000 % 60;
        millisecond = System.currentTimeMillis() % 1000;

        window.drawStringCentered(String.format("%02d", hour)
                + ":" + String.format("%02d", minute)
                + ":" + String.format("%02d", second), CENTRE, 20);
        window.setColor(0, 0, 0);

        drawDial(window);
        drawHourPointer(window);
        drawMinutePointer(window);
        drawSecondPointer(window);
    }

    private static double calcX(double radius, double factor, double arc) {
        return CENTRE + radius * Math.sin(factor * Math.toRadians(arc));
    }

    private static double calcY(double radius, double factor, double arc) {
        return CENTRE - radius * Math.cos(factor * Math.toRadians(arc));
    }

    public static void drawDial(Window window) {
        int r1 = CENTRE - 25;
        int r2 = CENTRE - 50;
        int r3 = CENTRE - 75;
        int r4 = CENTRE - 95;
        window.drawCircle(CENTRE, CENTRE, r1);

        for (int i = 0; i <= 60; i += 1) {
            double x1 = calcX(r1, 6, i);
            double y1 = calcY(r1, 6, i);
            double x2 = calcX(r2, 6, i);
            double y2 = calcY(r2, 6, i);

            window.setStrokeWidth(2);
            window.drawLine(x1, y1, x2, y2);

            if (i % 5 == 0) {
                double x3 = calcX(r3, 6, i);
                double y3 = calcY(r3, 6, i);

                if (i != 0) {
                    double x4 = calcX(r4, 6, i);
                    double y4 = calcY(r4, 6, i);

                    window.setStrokeWidth(5);
                    window.drawLine(x1, y1, x3, y3);
                    window.setFontSize(20);
                    window.drawStringCentered(String.valueOf(i / 5), x4, y4 + 7);
                }
            }
        }
    }


    public static void drawSecondPointer(Window window) {
        long length = CENTRE - 70;
        double millisec = millisecond;

        double x = calcX(length, 6, second + millisec / 1000);
        double y = calcY(length, 6, second + millisec / 1000);

        window.setColor(255, 0, 0);
        window.fillCircle(CENTRE, CENTRE, 7.5);
        window.setStrokeWidth(4);
        window.drawLine(CENTRE, CENTRE, x, y);
    }


    public static void drawMinutePointer(Window window) {
        int length = CENTRE - 50;
        double sec = second;

        double x = calcX(length, 6, minute + sec / 60);
        double y = calcY(length, 6, minute + sec / 60);

        window.setColor(0, 0, 0);
        window.fillCircle(CENTRE, CENTRE, 15);
        window.setStrokeWidth(10);
        window.drawLine(CENTRE, CENTRE, x, y);
    }


    public static void drawHourPointer(Window window) {
        int length = CENTRE - 125;
        double min = minute;

        double x = calcX(length, 30, hour + min / 60);
        double y = calcY(length, 30, hour + min / 60);

        window.setColor(0, 0, 0);
        window.fillCircle(CENTRE, CENTRE, 15);
        window.setStrokeWidth(10);
        window.drawLine(CENTRE, CENTRE, x, y);
    }

    public static int getSIZE() {
        return SIZE;
    }
}
