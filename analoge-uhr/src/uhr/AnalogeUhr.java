package uhr;

import gui.Window;

public class AnalogeUhr {
    private static final int SIZE = Watch.getSIZE();

    public static void main(String[] args) {
        Window window = new Window("Uhr", SIZE, SIZE);
        window.open();

        while (window.isOpen()) {
            Watch.drawWatch(window);
            window.refreshAndClear();
        }
    }
}
