package barchartrace;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class AllBars {
    private static final String DELIMITER = ",";
    private final Map<String, List<Bar>> barMap = new TreeMap<>();
    private final List<String> header = new ArrayList<>();
    private static final int LIMIT = 10;

    public AllBars(String fileName) {
        readFile(fileName);
        sortLists();
    }

    private void readFile(String fileName) {
        try(BufferedReader reader = getReader(fileName)) {
            header.add(reader.readLine());
            header.add(reader.readLine());
            header.add(reader.readLine());

            String line = reader.readLine();
            while (line != null) {
                if (line.split(DELIMITER).length == 5) {
                    String currentKey = line.split(DELIMITER)[0];
                    barMap.putIfAbsent(currentKey, new ArrayList<>());
                    barMap.get(currentKey).add(new Bar(line.split(DELIMITER)));
                }
                line = reader.readLine();
            }

        } catch (IOException e) {
            throw new IllegalStateException("failed");
        }
    }

    private void sortLists() {
        for (List<Bar> list : barMap.values()) {
            List<Bar> tempList = list.stream()
                    .sorted(Comparator.comparing(Bar::getValue).reversed())
                    .limit(LIMIT)
                    .toList();
            list.clear();
            list.addAll(tempList);
        }
    }

    private BufferedReader getReader(String fileName) {
        InputStream inputStream = getClass().getResourceAsStream(fileName);
        assert inputStream != null;
        InputStreamReader reader = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
        return new BufferedReader(reader);
    }

    public Map<String, List<Bar>> getBarMap() {
        return barMap;
    }

    public List<String> getHeader() {
        return header;
    }

    public int getLimit() {
        return LIMIT;
    }

}
