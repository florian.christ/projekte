package barchartrace;

public class Bar {

    private String name;
    private int value;
    private String category;

    public Bar(String[] line) {
        name = line[1];
        value = Integer.parseInt(line[3]);
        category = line[4];
    }

    public String toString() {
        return name + " " + value + " " + category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
