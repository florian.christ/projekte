package barchartrace;

import gui.Window;

import java.util.List;
import java.util.Map;

public class BarChartRace {
    private static final String FILE_NAME = "endgame.txt";
    private static final int WIDTH = 1000;
    private static final int HEIGHT = 650;
    private static final AllBars allBars = new AllBars(FILE_NAME);
    public static void main(String[] args) {

        Window window = new Window("Barchart Race", WIDTH, HEIGHT);
        window.open();


        for (Map.Entry<String, List<Bar>> entry : allBars.getBarMap().entrySet()) {

            window.setFontSize(20);
            window.setColor(0, 0, 0);
            window.drawStringCentered(allBars.getHeader().get(0), (double) WIDTH / 2, 25);
            window.drawStringCentered(entry.getKey(), (double) WIDTH / 2, HEIGHT - 25);

            drawBars(window, entry.getValue());

            window.refreshAndClear(5);

        }
        window.waitUntilClosed();

    }

    public static void drawBars(Window window, List<Bar> bars) {

        int hundredPercent = bars.get(0).getValue();
        int i = 120;

        for (int j = 50; j < hundredPercent; j += hundredPercent / 10) {
            double position = (double) (WIDTH - 20) / 100 * ((double) 100 / hundredPercent * j);
            window.setStrokeWidth(2);
            window.setColor(128, 128, 128);
            window.setFontSize(10);
            window.drawLine(position, 80, position, HEIGHT - 70 );
            window.drawStringCentered(String.valueOf(j), position , 70);
        }

        for (Bar b : bars) {
            if (b.getValue() > 0) {
                double length = (double) (WIDTH - 20) / 100 * ((double) 100 / hundredPercent * b.getValue());
                window.setStrokeWidth(40);
                window.setColor(140, 101, 211);
                window.drawLine(20, i, length, i);
                window.setColor(255, 255, 255);
                window.drawString(b.getName(), 40, i + 8);
                i += 45;
            }
        }

    }
}
