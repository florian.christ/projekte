package asciiart;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class AsciiArtist {
    private static final Map<Integer, String> symbols = new HashMap<>();

    public static void main(String[] args) {
        readInput();
        printSymbols();
    }

    public static void readInput() {
        Scanner in = new Scanner(System.in);

        String s = in.nextLine();

        for(char c : s.toCharArray()) {
            readFile(String.format("%02x", (int) c) + ".txt");
        }
    }

    private static void printSymbols() {
        for (String s : symbols.values()) {
            System.out.println(s);
        }
    }

    private static void readFile(String file) {
        try(BufferedReader reader = getReader(file)) {
            MutableInt count = new MutableInt(0);
            reader.lines()
                    .forEach(l -> {
                        symbols.merge(count.getValue(), l, (a, b) -> a + b);
                        count.increment();
                    });
        } catch (IOException e) {
            throw new IllegalStateException("failed");
        }
    }

    private static BufferedReader getReader(String fileName) {
        InputStream inputStream = AsciiArtist.class.getResourceAsStream(fileName);
        InputStreamReader reader = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
        return new BufferedReader(reader);
    }

    private static class MutableInt {
        int value;

        MutableInt(int value) {
            this.value = value;
        }

        void increment() {
            value++;
        }

        int getValue() {
            return value;
        }
    }
}
